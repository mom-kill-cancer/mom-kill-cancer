==================
Blood General Test
==================


Absolute Lymphocyte Count (x10^3/uL)
====================================


.. figure:: /src/plots/blood_general_test/Absolute_Lymphocyte_Count.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Absolute Neutrophil Count (x10^3/uL)
====================================


.. figure:: /src/plots/blood_general_test/Absolute_Neutrophil_Count.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Abnormal Lymphoid cell (%)
==========================


.. figure:: /src/plots/blood_general_test/Abnormal_Lymphoid_cell.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Atypical Lymphocyte (%)
=======================


.. figure:: /src/plots/blood_general_test/Atypical_Lymphocyte.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Band neutrophil (%)
===================


.. figure:: /src/plots/blood_general_test/Band_neutrophil.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Basophil (%)
============


.. figure:: /src/plots/blood_general_test/Basophil.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Blast (%)
=========


.. figure:: /src/plots/blood_general_test/Blast.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Eosinophil (%)
==============


.. figure:: /src/plots/blood_general_test/Eosinophil.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Hematocrit, Blood (%)
=====================


.. figure:: /src/plots/blood_general_test/Hematocrit,_Blood.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Hemoglobin, Blood (g/dL)
========================


.. figure:: /src/plots/blood_general_test/Hemoglobin,_Blood.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Immature cell (%)
=================


.. figure:: /src/plots/blood_general_test/Immature_cell.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Lymphocyte (%)
==============


.. figure:: /src/plots/blood_general_test/Lymphocyte.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Mean Corpuscular Hemoglobin (pg)
================================


.. figure:: /src/plots/blood_general_test/Mean_Corpuscular_Hemoglobin.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Mean Corpuscular Hemoglobin Concentration (g/dL)
================================================


.. figure:: /src/plots/blood_general_test/Mean_Corpuscular_Hemoglobin_Concentration.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Mean Corpuscular Volume (fL)
============================


.. figure:: /src/plots/blood_general_test/Mean_Corpuscular_Volume.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Metamyelocyte (%)
=================


.. figure:: /src/plots/blood_general_test/Metamyelocyte.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Monocyte (%)
============


.. figure:: /src/plots/blood_general_test/Monocyte.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Myelocyte (%)
=============


.. figure:: /src/plots/blood_general_test/Myelocyte.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Nucleated RBC (/100WBC)
=======================


.. figure:: /src/plots/blood_general_test/Nucleated_RBC.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Plasma cell (%)
===============


.. figure:: /src/plots/blood_general_test/Plasma_cell.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Platelet Count, Blood (x10^3/uL)
================================


.. figure:: /src/plots/blood_general_test/Platelet_Count,_Blood.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Promyelocyte (%)
================


.. figure:: /src/plots/blood_general_test/Promyelocyte.png
    :align: center
    :alt: alternate text
    :figclass: align-center

RBC Count, Blood (x10^3/uL)
===========================


.. figure:: /src/plots/blood_general_test/RBC_Count,_Blood.png
    :align: center
    :alt: alternate text
    :figclass: align-center

Segmented neutrophil (%)
========================


.. figure:: /src/plots/blood_general_test/Segmented_neutrophil.png
    :align: center
    :alt: alternate text
    :figclass: align-center

WBC count, Blood (x10^3/uL)
===========================


.. figure:: /src/plots/blood_general_test/WBC_count,_Blood.png
    :align: center
    :alt: alternate text
    :figclass: align-center
