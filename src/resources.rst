========
Resource
========

Web
===
- https://www.cancer.org
- https://www.healthline.com

Books
=====
- *암치유 생활백과* by 삼성서울병원
- `Caregiving for your loved one with cancer <https://media.cancercare.org/publications/original/1-ccc_caregiver.pdf>`_ by www.cancercare.org
- *암에 대해 알아야 할 모든 것* by 서울대학교병원
- *생로병사의 비밀 10년의 기록. 암중모색 암을 이긴 사람들의 비밀* by KBS