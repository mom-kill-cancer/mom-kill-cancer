============
Side Effects
============
This page contains general side effects regarding cancer treatments. `Source: American Cancer Society "treatments and side effects" <https://www.cancer.org/treatment/treatments-and-side-effects/physical-side-effects.html>`_.


Chemo Brain [1]_
================

The symptoms of chemo brain:

- Forgetting things that they usually have no trouble recalling (memory lapses)
- Trouble concentrating (they can’t focus on what they’re doing, have a short attention span, may “space out”)
- Trouble remembering details like names, dates, and sometimes larger events
- Trouble multi-tasking, like answering the phone while cooking, without losing track of one task (they’re less able to do more than one thing at a time)
- Taking longer to finish things (disorganized, slower thinking and processing)
- Trouble remembering common words (unable to find the right words to finish a sentence)

Causes:

- The cancer itself
- Other drugs used as part of treatment, such as steroids, anti-nausea, or pain medicines
- Surgery and the drugs used during surgery (anesthesia)
- Low blood counts
- Sleep problems
- Infection
- Tiredness (fatigue)
- Hormone changes or hormone treatments
- Other illnesses, such as diabetes or high blood pressure
- Nutritional deficiencies
- Patient age
- Depression
- Stress, anxiety, worry, or other emotional pressure


Clinical Depression [2]_
========================

Confusion [3]_
==============

Anxiety, Fear, and Emotional Distress [4]_
==========================================


.. rubric:: References

.. [1] https://www.cancer.org/treatment/treatments-and-side-effects/physical-side-effects/changes-in-mood-or-thinking/chemo-brain.html
.. [2] https://www.cancer.org/treatment/treatments-and-side-effects/physical-side-effects/changes-in-mood-or-thinking/depression.html
.. [3] https://www.cancer.org/treatment/treatments-and-side-effects/physical-side-effects/changes-in-mood-or-thinking/confusion.html
.. [4] https://www.cancer.org/treatment/treatments-and-side-effects/physical-side-effects/changes-in-mood-or-thinking/anxiety-and-fear.html
