===============
Treatment Types
===============
Information about different cancer treatments.

This page is a summary of `American Cancer Society <https://www.cancer.org/treatment/treatments-and-side-effects/treatment-types.html>`_.

.. list-table::

	- - **Common Treatments**

		- Surgery
		- Chemotherapy
		- Radiation
		- Targeted Therapy
		- Immunotherapy

	  - **Other Procedures and Techniques**

		- Stem Cell Transplant
		- Hyperthermia
		- Photodynamic Therapy
		- Blood Transfusion & Donation
		- Lasers in Cancer Treatment



Common Treatments
=================

Surgery
#######

Chemotherapy
############

Radiation
#########

Targeted Therapy
################
표적항암제. `Source: American Cancer Society <https://www.cancer.org/treatment/treatments-and-side-effects/treatment-types/targeted-therapy/what-is.html>`_

Targeted drugs can work to:

- **Block or turn off chemical signals** that tell the cancer cell to grow and divide
- **Change proteins** within the cancer cells so the cells die
- **Stop making new blood vessels** to feed the cancer cells
- **Trigger your immune system** to kill the cancer cells
- **Carry toxins to the cancer cells** to kill them, but not normal cells

Epidermal Growth Factor Receptor(EGFR) inhibitors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`상피세포 성장인자 수용체 억제 약제 <https://www.cancer.org/cancer/non-small-cell-lung-cancer/treating/targeted-therapies.html>`_

- 이레사
- 타세바
- 지오트립

PD1/PD-L1 inhibitors
^^^^^^^^^^^^^^^^^^^^
`면역 표적항암제 억제 약제 <https://nyulangone.org/conditions/non-small-cell-lung-cancer/treatments/chemotherapy-targeted-drugs-for-lung-cancer>`_

PD-1
****
- Nivolumab (옵디보; Opdivo)
- Pembrolizumab (키트루다; Keytruda)

.. figure:: /images/chemotherapy/opdivo_for_NSCLC.png
	:scale: 25%
	:align: center
	:alt: alternate text
	:figclass: align-center


Recently permitted chemo for non-small cell lung cancer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- 타그리쏘(Osimertinib)
- 올리타(Olmutinib)
- 자이카디아(Ceritinib)
- 알레센사(Alectinib)
- 옵디보(Nivolumab)
- 키트루다(Pembrolizumab)
- 포트라자(Necitumumab)

.. note::

	- 약제 반응이 좋은 환자들을 구별할 수 있는 검사 실시 후 치료여부 결정

		- 타그리쏘/올리타: EGFR 유전자 돌연변이 검사
		- 키트루다: PD-L1 검사

	- Does have side effects
	- Expensive. 초기에는 보험급여 대상이 되지 않음




Side effect
^^^^^^^^^^^
피부발진, 설사

.. figure:: /images/chemotherapy/chemo_side_effect.jpg
   :align: center
   :alt: alternate text
   :figclass: align-center



Immunotherapy
#############
d

--------

Other Procedures and Techniques
===============================

Stem Cell Transplant
####################

Hyperthermia
############
고주파온열치료. It is a carefully controlled use of heat for medical purposes. When cells in the body are exposed to higher than normal temperatures, changes take place inside the cells. These changes can make the cells more likely to be affected by other treatments such as radiation therapy or chemotherapy. Very high temperatures can kill cancer cells outright (thermal ablation), but they also can injure or kill normal cells and tissues. This is why hyperthermia must be carefully controlled and should be done by doctors who are experienced in using it.

Current instruments can deliver heat precisely, and hyperthermia is being used (or studied for use) against many types of cancer. [1]_

.. note::

	남양주 `에덴요양병원에서 고주파온열치료 서비스 제공 <http://edenah.com/ko/center/eden-highfrqeucy/>`_.

Photodynamic Therapy
####################

Blood Transfusion & Donation
############################

Lasers in Cancer Treatment
##########################

.. rubric:: References
.. [1] https://www.cancer.org/treatment/treatments-and-side-effects/treatment-types/hyperthermia.html