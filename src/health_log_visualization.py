#!/usr/bin/python3

import plotly.plotly as py              # for sending things to plotly
import plotly.tools as tls              # for mpl, config, etc.
import plotly.graph_objs as go         # __all__ is safely defined
import plotly

print(plotly.__version__)

import pandas as pd
import numpy as np

# Offline mode
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
init_notebook_mode(connected=True)



ADVERSE_LEVEL_MAX = 10
FILLCOLOR_TRACE_CHEMO = 'rgba(194,130,100,0.2)'
COLOR_WHITE = 'rgba(255,255,255,0)'


class HealthLogPlotMaker:
    
    def __init__(self,csv_name, plot_filename):
        self.df = pd.read_csv(csv_name, parse_dates=['Date'])
        self.df = self.df.assign(DateSimple=self.df.Date.dt.strftime('%b/%d/%a'))
        self.plot_filename = plot_filename
        self.rst_writer = RSTWriter(
            'Health Condition Log', 
            'health_log.rst', 
            'summaries/period1.rst', 
            plot_filename
        )


    def get_trace_adverse_effects(self, df):
        adverse_effects = [
            'diarrhea',
            'sore_mouth',
            'fatigue',
            'nausea',
            'vomitting',
            'constipation',
            'loss_of_appetite',
            'pain_with_swallowing',
            'swelling_limbs',
            'itching',
            'rash',
            'shortness_of_breath',
            'cough',
            'muscle_pain',
            'joint_pain',
            'numbness_or_tingling_in_limbs',
            'anemia'
        ]

        positive_adverse_effects = [
            ae for ae in adverse_effects if df[ae].max() > 0 
        ]
        
        # Plot only since the chemotherapy
        df = df[
            df.Date >= df.Date[
                df.chemo.eq(True).idxmax()
            ]
        ]

        trace_chemo = self.get_trace_chemo(df)

        data = []
        line_style = [None,dict(dash='dot'),dict(dash='dash')]
        for i in range(len(positive_adverse_effects)):
            trace = go.Scatter(
                x = df.DateSimple,
                y = df[positive_adverse_effects[i]],
                name = positive_adverse_effects[i],
                line = line_style[i%len(line_style)]
            )

            data.append(trace)
            
        data.append(trace_chemo)


        # Edit the layout
        layout = go.Layout(
            title = 'Chemo period 1 – Adverse Effects',
            yaxis = dict(
                title = 'Adverse level',
                fixedrange=True
                ),
            xaxis = dict(
                fixedrange=True
                ),
            
        )



        fig = go.Figure(data=data, layout=layout)
        return plot(
            fig, 
            include_plotlyjs=False, 
            output_type='div',
            config={'showLink': False}
        )
        
    def get_trace_chemo(self, df, yaxis='y'):
        X = df[(df.chemo == True)].DateSimple
        Y = [ADVERSE_LEVEL_MAX for _ in range(len(X))]

        return go.Scatter(
            x=X,
            y=Y,
            fill='tozeroy',
            fillcolor=FILLCOLOR_TRACE_CHEMO,
            line=dict(color=COLOR_WHITE),
            #         showlegend=False,
            name='on chemo',
            yaxis=yaxis
        )


    def get_traces_for_feature(self, df, feature):
        trace_chemo = self.get_trace_chemo(df,yaxis='y2')

        trace_feature = go.Scatter(
            x = df.DateSimple,
            y = df[feature],
            name = feature,
            line = dict(
                color = ('rgb(205, 12, 24)'),
                width = 6)
        )

        trace_feature_mean = go.Scatter(
            x = df.DateSimple,
            y = [df[feature].mean()] * len(df.Date),
            name = '{} mean'.format(feature),
            line = dict(
                color = ('rgb(167, 230, 20)'),
                width = 2,
            ),
            mode='lines'
        )

        trace_adverse_level = go.Scatter(
            x=df.DateSimple,
            y=df.adverse_level,
            name='Adverse level',
            yaxis='y2',
            line = dict(
                color='rgb(148, 103, 189)',
                width = 2,
                dash = 'dash'
            ),
        )

        return [
            trace_feature_mean,
            trace_feature,
            trace_adverse_level,
            trace_chemo,
        ]


    def get_feature_plot(self, df, feature):
        data = self.get_traces_for_feature(df, feature)

        # Edit the layout
        layout = go.Layout(
            title = 'Chemo period 1 – {} & adverse level'.format(feature),
            # xaxis = dict(title = 'Date'),
            yaxis = dict(
                title = 'Weight (kg)' if feature == 'weight' else 'Number of steps' ,
                titlefont=dict(
                    size=15
                ),
                tickfont=dict(
                    size=14
                ),
                fixedrange=True
            ),
            yaxis2=dict(
                title='Adverse level',
                titlefont=dict(
                    color='rgb(148, 103, 189)',
                    size=14
                ),
                tickfont=dict(
                    color='rgb(148, 103, 189)',
                    size=14
                ),
                overlaying='y',
                side='right',
                fixedrange=True
            ),
            legend=dict(
                # x=0,
                # y=1,
                x=-.1,
                y=1.35,
                # traceorder='normal',
                # font=dict(
                #     family='sans-serif',
                #     size=12,
                #     color='#000'
                # ),
                # bgcolor='#E2E2E2',
                # bordercolor='#FFFFFF',
                # borderwidth=2
            ),
            xaxis=dict(
                fixedrange=True
            )
        )

        config={'showLink': False}
        fig = go.Figure(data=data, layout=layout)
        p =  plot(
            fig, 
            include_plotlyjs=False, 
            output_type='div',
            config=config
        )
        return p
        # self.rst_writer.add_plotly_plot(p)


    def get_plots(self):
        weight_plot = self.get_feature_plot(self.df, 'weight')
        self.rst_writer.add_plotly_plot(weight_plot)


        steps_plot = self.get_feature_plot(self.df, 'steps')
        self.rst_writer.add_plotly_plot(steps_plot)

        adverse_plot = self.get_trace_adverse_effects(self.df)
        self.rst_writer.add_plotly_plot(adverse_plot)

    # def get_plots(self):
    #     self.get_feature_plot(self.df, 'weight')
        # self.rst_writer.add_plotly_plot(weight_plot)

        # self.rst_writer.add_plotly_plot(steps_plot)
        # self.get_feature_plot(self.df, 'steps')




class RSTPlotWriter:
    
    RAW_HTML_BLOCK = '.. raw:: html\n\n\t<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>'
    # RAW_HTML_BLOCK = '.. raw:: html\n\n\t<script src="https://gist.githubusercontent.com/SuperShinyEyes/c075bd4dbfcf34d77a237b94ee1ac738/raw/789fb156d02c586713465035e6401ffc465a7992/plotly-20180807.min.js"></script>'
    # RAW_HTML_BLOCK = '.. raw:: html\n\n\t<script src="../../../plotly-latest.min.js"></script>'
    # RAW_HTML_BLOCK = '.. raw:: html\n\n'

    def __init__(self, filename):
        self.filename = filename
        self.create_file()
        
        
    def create_file(self):
        with open(self.filename, 'w') as f:
            f.write('')
        
    def write_plotly_plot(self, plot_div):
        with open(self.filename, 'a') as f:
            
            content = '{raw_html_block}\n\n\t{plot_div}\n\n'.format(
                raw_html_block = RSTPlotWriter.RAW_HTML_BLOCK,
                plot_div = plot_div
            )        
            f.write(content)

        
    
class RSTWriter:
    
    
    def __init__(self, title, filename, summary_filename, plot_filename):
        self.title = title
        self.filename = filename
        self.plot_filename = plot_filename
        self.summary_filename = summary_filename
        self.rst_plot_writer = RSTPlotWriter(plot_filename)
        self.write_title()

        
    def write_title(self):
        with open(self.filename, 'w') as f:
            line = '=' * len(self.title)
            f.write(
                '{line}\n{title}\n{line}\n\n\n'.format(
                    line=line, 
                    title=self.title
                )
            )
            f.write('.. include :: {}\n\n'.format(self.summary_filename))
            f.write('.. include :: {}\n\n'.format(self.plot_filename))
        
    def add_plotly_plot(self, plot_div):
        self.rst_plot_writer.write_plotly_plot(plot_div)
            
        


    
hl = HealthLogPlotMaker('data/health_condition_log.csv', 'plots/period1.rst')
hl.get_plots()