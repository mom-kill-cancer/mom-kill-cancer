
===================
Blood Chemical Test
===================


A/G ratio
=========

.. figure:: /src/plots/blood_chemical_test/A_G_ratio.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


ALP (U/l)
=========

.. figure:: /src/plots/blood_chemical_test/ALP.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


ALT (U/l)
=========

.. figure:: /src/plots/blood_chemical_test/ALT.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


AST (U/l)
=========

.. figure:: /src/plots/blood_chemical_test/AST.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Albumin (g/dl)
==============

.. figure:: /src/plots/blood_chemical_test/Albumin.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


BUN  (mg/dl)
============

.. figure:: /src/plots/blood_chemical_test/BUN.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


BUN & Creatinine ratio
======================

.. figure:: /src/plots/blood_chemical_test/BUN_&_Creatinine_ratio.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Bilirubin, Total (mg/dl)
========================

.. figure:: /src/plots/blood_chemical_test/Bilirubin,_Total.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Ca  (mg/dl)
===========

.. figure:: /src/plots/blood_chemical_test/Ca.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Cholesterol (mg/dl)
===================

.. figure:: /src/plots/blood_chemical_test/Cholesterol.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Creatinine  (mg/dl)
===================

.. figure:: /src/plots/blood_chemical_test/Creatinine.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Estimated GFR (mL/min/1.73m^2)
==============================

.. figure:: /src/plots/blood_chemical_test/Estimated_GFR.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Globulin  (g/dl)
================

.. figure:: /src/plots/blood_chemical_test/Globulin.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Glucose, Fasting  (mg/dl)
=========================

.. figure:: /src/plots/blood_chemical_test/Glucose,_Fasting.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


P  (mg/dl)
==========

.. figure:: /src/plots/blood_chemical_test/P.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Protein, Total  (g/dl)
======================

.. figure:: /src/plots/blood_chemical_test/Protein,_Total.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    


Uric Acid  (mg/dl)
==================

.. figure:: /src/plots/blood_chemical_test/Uric_Acid.png
    :align: center
    :alt: alternate text
    :figclass: align-center
    

