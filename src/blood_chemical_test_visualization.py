#!/usr/bin/python3

import plotly.plotly as py              # for sending things to plotly
import plotly.tools as tls              # for mpl, config, etc.
import plotly.graph_objs as go         # __all__ is safely defined
import plotly

print(plotly.__version__)

import pandas as pd
import numpy as np

# Offline mode
import plotly.plotly as py              # for sending things to plotly
import plotly.tools as tls              # for mpl, config, etc.
import plotly.graph_objs as go         # __all__ is safely defined
import plotly.io as pio
import plotly
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
init_notebook_mode(connected=True)


class TestItem(object):
    def __init__(
        self, 
        name_full,
        lower_bound=0,
        upper_bound=0,
        ):
        self.name_full = name_full
        self.name_short, self.unit = self.__get_name_and_unit()
        self.upper_bound = upper_bound
        self.lower_bound = lower_bound

    def __get_name_and_unit(self):
        try:
            i = self.name_full.index('(')
        except ValueError:
            name = self.name_full
            unit = None
        else:
            name = self.name_full[:i].strip()
            e = self.name_full.index(')')
            unit = self.name_full[i+1:e]
        finally:
            return name, unit

    def is_out_of_bounds(self):
        return self.value < self.lower_bound or self.value > upper_bound
        
        
test_items = [
    TestItem(name_full="A/G ratio", lower_bound=1.3, upper_bound=2.2),
    TestItem(name_full="ALP (U/l)", lower_bound=35, upper_bound=104),
    TestItem(name_full="ALT (U/l)", lower_bound=0, upper_bound=33),
    TestItem(name_full="AST (U/l)", lower_bound=0, upper_bound=32),
    TestItem(name_full="Albumin (g/dl)", lower_bound=3.5, upper_bound=5.2),
    TestItem(name_full="BUN  (mg/dl)", lower_bound=6, upper_bound=23),
    TestItem(name_full="BUN & Creatinine ratio", lower_bound=13.3, upper_bound=20.0),
    TestItem(name_full="Bilirubin, Total (mg/dl)", lower_bound=0, upper_bound=1.2),
    TestItem(name_full="Ca  (mg/dl)", lower_bound=9.3, upper_bound=177),
    TestItem(name_full="Cholesterol (mg/dl)", lower_bound=0, upper_bound=200),
    TestItem(name_full="Creatinine  (mg/dl)", lower_bound=0.5, upper_bound=0.9),
    TestItem(name_full="Estimated GFR (mL/min/1.73m^2)", lower_bound=60, upper_bound=150),
    TestItem(name_full="Globulin  (g/dl)", lower_bound=2, upper_bound=3.5),
    TestItem(name_full="Glucose, Fasting  (mg/dl)", lower_bound=74, upper_bound=109),
    TestItem(name_full="P  (mg/dl)", lower_bound=2.5, upper_bound=4.5),
    TestItem(name_full="Protein, Total  (g/dl)", lower_bound=6.4, upper_bound=8.3),
    TestItem(name_full="Uric Acid  (mg/dl)", lower_bound=2.4, upper_bound=5.7),
]

def get_image_name(name_short):
    path = "plots/blood_chemical_test/" 
    name_short = name_short.replace('/', '_')
    return path + '_'.join(name_short.split()) + ".png"


def make_plot(df, test_item):
    data = [
        # Result Value Trace
        go.Scatter(
            x = df.DateSimple,
            y = df[test_item.name_full],
            # name = test_item.name_short,     # TOO LONG!!!
            name = "result",
            line = dict(color="green", width=2)
        ),

    ]

    if test_item.lower_bound < test_item.upper_bound:
        data.append(
                go.Scatter(
                    x = df.DateSimple,
                    y = [test_item.upper_bound] * len(df.DateSimple),
                    name = "upper",
                    line = dict(dash='dot', color="red", width=2)
                )
            )

        if test_item.lower_bound != 0:
            data.append(
                go.Scatter(
                    x = df.DateSimple,
                    y = [test_item.lower_bound] * len(df.DateSimple),
                    name = "lower",
                    line = dict(dash='dot', color="red", width=2)
                )
            )
            


    layout = go.Layout(
        title = 'Chemical Blood Test - {:s}'.format(test_item.name_full),
        font=dict(size=18),
        yaxis = dict(
            title = test_item.name_full,
            fixedrange=True
            ),
        xaxis = dict(
            fixedrange=True
            ),
    )

    fig = go.Figure(data=data, layout=layout)

    image_name = get_image_name(test_item.name_short)

    pio.write_image(fig, image_name, format="png", scale=None, width=1000, height=500, validate=True)
    
    


def main():
    CSV_PATH = "data/health_conditioin_log - blood_chemical.csv"
    df = pd.read_csv(CSV_PATH, parse_dates=['Date'])
    df = df.drop(df.index[0])
    df = df.assign(DateSimple=df.Date.dt.strftime('%b/%d/%a'))
    df = df.set_index('Date')

    for test_item in test_items:
        make_plot(df, test_item)

if __name__ == "__main__":
    main()


    