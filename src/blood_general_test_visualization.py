#!/usr/bin/python3

import plotly.plotly as py              # for sending things to plotly
import plotly.tools as tls              # for mpl, config, etc.
import plotly.graph_objs as go         # __all__ is safely defined
import plotly

print(plotly.__version__)

import pandas as pd
import numpy as np

# Offline mode
import plotly.plotly as py              # for sending things to plotly
import plotly.tools as tls              # for mpl, config, etc.
import plotly.graph_objs as go         # __all__ is safely defined
import plotly.io as pio
import plotly
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
init_notebook_mode(connected=True)


class TestItem(object):
    def __init__(
        self, 
        name_full,
        lower_bound=0,
        upper_bound=0,
        ):
        self.name_full = name_full
        self.name_short, self.unit = self.__get_name_and_unit()
        self.upper_bound = upper_bound
        self.lower_bound = lower_bound

    def __get_name_and_unit(self):
        i = self.name_full.index('(')
        name = self.name_full[:i].strip()
        e = self.name_full.index(')')
        unit = self.name_full[i+1:e]
        return name, unit

    def is_out_of_bounds(self):
        return self.value < self.lower_bound or self.value > upper_bound
        
        
test_items = [
    TestItem(name_full='Absolute Lymphocyte Count (x10^3/uL)', lower_bound=1.0, upper_bound=4.8),
    TestItem(name_full='Absolute Neutrophil Count (x10^3/uL)',  lower_bound=1.57, upper_bound=8.3),
    TestItem(name_full='Abnormal Lymphoid cell (%)', lower_bound=0, upper_bound=0),
    TestItem(name_full='Atypical Lymphocyte (%)', lower_bound=0, upper_bound=0),
    TestItem(name_full='Band neutrophil (%)', lower_bound=0, upper_bound=0),
    TestItem(name_full='Basophil (%)', lower_bound=0, upper_bound=1.6),
    TestItem(name_full="Blast (%)", lower_bound=0, upper_bound=0),
    TestItem(name_full="Eosinophil (%)", lower_bound=0, upper_bound=8.6),
    TestItem(name_full="Hematocrit, Blood (%)", lower_bound=31.8, upper_bound=43.8),
    TestItem(name_full="Hemoglobin, Blood (g/dL)", lower_bound=11.2, upper_bound=14.8),
    TestItem(name_full="Immature cell (%)", lower_bound=0, upper_bound=0),
    TestItem(name_full="Lymphocyte (%)", lower_bound=20, upper_bound=50.8),
    TestItem(name_full="Mean Corpuscular Hemoglobin (pg)", lower_bound=27.8, upper_bound=33.2),
    TestItem(name_full="Mean Corpuscular Hemoglobin Concentration (g/dL)", lower_bound=31.9, upper_bound=34.6),
    TestItem(name_full="Mean Corpuscular Volume (fL)", lower_bound=83.9, upper_bound=98.1),
    TestItem(name_full="Metamyelocyte (%)"),
    TestItem(name_full="Monocyte (%)", lower_bound=1.7, upper_bound=8),
    TestItem(name_full="Myelocyte (%)"),
    TestItem(name_full="Nucleated RBC (/100WBC)"),
    TestItem(name_full="Plasma cell (%)"),
    TestItem(name_full="Platelet Count, Blood (x10^3/uL)", lower_bound=138, upper_bound=347),
    TestItem(name_full="Promyelocyte (%)"),
    TestItem(name_full="RBC Count, Blood (x10^3/uL)", lower_bound=3.68, upper_bound=4.83),
    TestItem(name_full="Segmented neutrophil (%)", lower_bound=40.6, upper_bound=73.5),
    TestItem(name_full="WBC count, Blood (x10^3/uL)", lower_bound=3.15, upper_bound=8.63),
]

def make_plot(df, test_item):
    data = [
        # Result Value Trace
        go.Scatter(
            x = df.DateSimple,
            y = df[test_item.name_full],
            # name = test_item.name_short,     # TOO LONG!!!
            name = "result",
            line = dict(color="green", width=2)
        ),

    ]

    if test_item.lower_bound < test_item.upper_bound:
        data.append(
                go.Scatter(
                    x = df.DateSimple,
                    y = [test_item.upper_bound] * len(df.DateSimple),
                    name = "upper",
                    line = dict(dash='dot', color="red", width=2)
                )
            )

        if test_item.lower_bound != 0:
            data.append(
                go.Scatter(
                    x = df.DateSimple,
                    y = [test_item.lower_bound] * len(df.DateSimple),
                    name = "lower",
                    line = dict(dash='dot', color="red", width=2)
                )
            )
            


    layout = go.Layout(
        title = 'General Blood Test - {:s}'.format(test_item.name_full),
        font=dict(size=18),
        yaxis = dict(
            title = test_item.name_full,
            fixedrange=True
            ),
        xaxis = dict(
            fixedrange=True
            ),
    )

    fig = go.Figure(data=data, layout=layout)

    image_name = "plots/blood_general_test/" + '_'.join(test_item.name_short.split()) + ".png"

    pio.write_image(fig, image_name, format="png", scale=None, width=1000, height=500, validate=True)
    
    


def main():
    CSV_PATH = "data/health_conditioin_log - blood_general.csv"
    df = pd.read_csv(CSV_PATH, parse_dates=['Date'])
    df = df.drop(df.index[0])
    df = df.assign(DateSimple=df.Date.dt.strftime('%b/%d/%a'))
    df = df.set_index('Date')

    for test_item in test_items:
        make_plot(df, test_item)

if __name__ == "__main__":
    main()


    