==========
Tumor Test
==========

CA 19-9 (U/ml)
==============


.. figure:: /src/plots/tumor_test/CA_19-9.png
    :align: center
    :alt: alternate text
    :figclass: align-center


CEA (ng/ml)
===========

.. figure:: /src/plots/tumor_test/CEA.png
    :align: center
    :alt: alternate text
    :figclass: align-center
