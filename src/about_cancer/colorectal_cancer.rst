=================
Colorectal Cancer
=================

Treatment
=========

Treating stage IV rectal cancer
###############################
Stage IV rectal cancers have spread to distant organs and tissues such as the liver or lungs. Treatment options for stage IV disease depend to some extent on how widespread the cancer is.

If the cancer is more widespread and can’t be removed completely by surgery, the cancer will likely be treated with chemo and/or targeted therapy drugs (without surgery). Some of the options include:

- FOLFOX: leucovorin, 5-FU, and oxaliplatin (Eloxatin)
- FOLFIRI: leucovorin, 5-FU, and irinotecan (Camptosar)
- CAPEOX or CAPOX: capecitabine (Xeloda) and oxaliplatin
- FOLFOXIRI: leucovorin, 5-FU, oxaliplatin, and irinotecan
- One of the above combinations, plus either a drug that targets VEGF (bevacizumab [Avastin], ziv-aflibercept [Zaltrap], or ramucirumab [Cyramza]), or a drug that targets EGFR (cetuximab [Erbitux] or panitumumab [Vectibix])
- 5-FU and leucovorin, with or without a targeted drug
- Capecitabine, with or without a targeted drug
- Irinotecan, with or without a targeted drug
- Cetuximab alone
- Panitumumab alone
- Regorafenib (Stivarga) alone
- Trifluridine and tipiracil (Lonsurf)

If chemo shrinks the tumors, in some cases it may be possible to consider surgery to try to remove all of the cancer at this point. Chemo may then be given again after surgery.

If the tumor doesn't shrink, a different drug combination may be tried. For people with certain gene changes in their cancer cells, another option after initial chemotherapy might be treatment with an immunotherapy drug such as pembrolizumab (Keytruda).


Treating distant recurrence
###########################

If the cancer has spread too much to be treated with surgery, chemo and/or targeted therapies may be used. Possible regimens are the same as for stage IV disease.

For people whose cancers are found to have certain traits on lab tests, another option might be treatment with immunotherapy.

Your options depend on which, if any, drugs you had before the cancer came back and how long ago you got them, as well as your overall health. You may still need surgery at some point to relieve or prevent blockage of the colon or other local problems. Radiation therapy may be an option to relieve symptoms as well.

Recurrent cancers can often be hard to treat, so you might also want to ask your doctor if clinical trials of newer treatments are available. [1]_

------

Targeted Therapy Drugs for Colorectal Cancer
============================================

Drugs that target blood vessel formation (VEGF)
###############################################
Vascular endothelial growth factor (VEGF) is a protein that helps tumors form new blood vessels (a process known as angiogenesis) to get nutrients they need to grow. Drugs that stop VEGF from working can be used to treat some colon or rectal cancers. These include:

- Bevacizumab (Avastin, **mom is taking this**) 
- Ramucirumab (Cyramza)
- Ziv-aflibercept (Zaltrap)

These drugs are given as infusions into your vein (IV) every 2 or 3 weeks, in most cases along with chemotherapy. When combined with chemo, these drugs can often help people with advanced colon or rectal cancers live longer.

Possible side effects of drugs that target VEGF
###############################################

Common side effects of these drugs include:

- High blood pressure
- Extreme tiredness (fatigue)
- Bleeding
- Low white blood cell counts (with increased risk of infections)
- Headaches
- Mouth sores
- Loss of appetite
- Diarrhea

Rare but possibly serious side effects include blood clots, severe bleeding, holes forming in the colon (called perforations), heart problems, kidney problems, and slow wound healing. If a hole forms in the colon it can lead to severe infection and surgery may be needed to fix it.

Drugs that target cells with EGFR changes
#########################################
Epidermal growth factor receptor (EGFR) is a protein that helps cancer cells grow. There's often a lot of it on the surface of cancer cells. Drugs that target EGFR can be used to treat some advanced colon or rectal cancers. These include:

- Cetuximab (Erbitux, **mom cannot take it due to her genetic mutation**)
- Panitumumab (Vectibix)

Both of these drugs are given by IV infusion, either once a week or every other week.

These drugs don't work in colorectal cancers that have mutations (defects) in the KRAS, NRAS or BRAF gene. Doctors now commonly test the tumor for these gene changes before treatment, and only use these drugs in people who don't have these mutations. 

Other targeted therapy drugs
############################
Regorafenib (Stivarga) is a type of targeted therapy known as a kinase inhibitor. Kinases are proteins on or near the surface of a cell that carry important signals to the cell’s control center. Regorafenib blocks several kinase proteins that either help tumor cells grow or help form new blood vessels to feed the tumor. Blocking these proteins can help stop the growth of cancer cells.

This drug is used to treat advanced colorectal cancer, typically when other drugs are no longer helpful. It's taken as a pill.

Common side effects include fatigue, loss of appetite, hand-foot syndrome (redness and irritation of the hands and feet), diarrhea, high blood pressure, weight loss, and abdominal pain.

Less common but more serious side effects can include severe bleeding or perforations (holes) in the stomach or intestines.

---------


.. rubric:: References
.. [1] https://www.cancer.org/cancer/colon-rectal-cancer/treating/by-stage-rectum.html